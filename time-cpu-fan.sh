#!/bin/zsh
LANG=C
FAN=$(sensors | grep fan2: | awk '{print $2;}')
CPU=$(sensors | grep Tctl: | awk '{printf "%4.1f\n",$2;}')
TIME=$(date +%H:%M:%S)
FAN_PSU=$(sensors | grep fan1:|awk '{print $2;}' | tail -1)

#CPUFREQ0=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq | gawk '{printf "%4.0f\n", $1/1000.}')
#CPUFREQ1=$(cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_cur_freq | gawk '{printf "%4.0f\n", $1/1000.}')
#CPUFREQ2=$(cat /sys/devices/system/cpu/cpu2/cpufreq/scaling_cur_freq | gawk '{printf "%4.0f\n", $1/1000.}')
#CPUFREQ3=$(cat /sys/devices/system/cpu/cpu3/cpufreq/scaling_cur_freq | gawk '{printf "%4.0f\n", $1/1000.}')
#CPUFREQ4=$(cat /sys/devices/system/cpu/cpu4/cpufreq/scaling_cur_freq | gawk '{printf "%4.0f\n", $1/1000.}')
#CPUFREQ5=$(cat /sys/devices/system/cpu/cpu5/cpufreq/scaling_cur_freq | gawk '{printf "%4.0f\n", $1/1000.}')
#CPUFREQ6=$(cat /sys/devices/system/cpu/cpu6/cpufreq/scaling_cur_freq | gawk '{printf "%4.0f\n", $1/1000.}')
#CPUFREQ7=$(cat /sys/devices/system/cpu/cpu7/cpufreq/scaling_cur_freq | gawk '{printf "%4.0f\n", $1/1000.}')

#echo "$TIME, $CPU, $FAN, $FAN_PSU, $CPUFREQ0, $CPUFREQ1, $CPUFREQ2, $CPUFREQ3, $CPUFREQ4, $CPUFREQ5, $CPUFREQ6, $CPUFREQ7"
echo "$TIME, $CPU, $FAN, $FAN_PSU"
