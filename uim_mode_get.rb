#!/usr/bin/env ruby

require "socket"

runtime_dir = ENV["XDG_RUNTIME_DIR"]
if runtime_dir
  base_dir = File.join(runtime_dir, "uim")
else
  base_dir = File.join(Dir.home, ".uim.d")
end
uim_helper_socket_path = File.join(base_dir, "socket", "uim-helper")

UNIXSocket.open(uim_helper_socket_path) do |socket|
  # See https://github.com/uim/uim/blob/master/doc/HELPER-PROTOCOL for
  # the helper protocol details.

  # Split by an empty line so that we can process each command.
  socket.each_line(chomp: true).chunk(&:empty?).each do |separator, lines|
    next if separator
    command = lines.shift
    case command
    when "prop_list_update"
      # charset=UTF-8 -> UTF-8
      encoding = lines.shift.split("=", 2)[1]
      indicator = +""
      indicator.force_encoding(encoding)
      lines.each do |line|
        line.force_encoding(encoding)
        # branch\t...
        # OR
        # leaf\t...
        parts = line.split("\t")
        part_type = parts.shift
        case part_type
        when "branch"
          # "mozc", "Mz", "Mozc"
          # "ja_direct", "-", "直接入力"
          # "ja_hiragana", "あ", "ひらがな"
          # ...
          indication_id, iconic_label, label_string = parts
          indicator << iconic_label
        end
      end
      # ファイルに書き出すようにする
      ore_file = File.open("/var/run/user/1000/uimmode", "w")
      ore_file.puts(indicator.encode("UTF-8"))
      ore_file.close
    end
  end
end

